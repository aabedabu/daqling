# Third-party dependencies

If `-DCERN_ENVIRONMENT=OFF` have been specified this directory will instead be used for uncommon third-party libraries.
Before that, however, this directory must be initialized: clone the repositories below.

    git clone --depth 1 https://github.com/gabime/spdlog.git --branch v1.3.1 spdlog
    git clone --depth 1 https://github.com/zeromq/libzmq.git --branch v4.3.2 zmq
    git clone --depth 1 https://github.com/whoshuu/cpr.git --branch 1.3.0 cpr
    git clone --depth 1 https://github.com/nlohmann/json.git --branch v3.7.0 json
    git clone --depth 1 https://github.com/zeromq/cppzmq.git --branch v4.3.0 cppzmq
    git clone --depth 1 https://github.com/nemequ/hedley.git --branch v9 hedley
