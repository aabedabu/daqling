# Define module
daqling_module(module_name)

# Add source file to library
daqling_target_sources(${module_name}
    PmemWriterModule.cpp
)

# Link library
target_link_libraries(${module_name}  /usr/lib64/libpmemblk.so /usr/lib64/libpmem.so )


# Provide install target
daqling_target_install(${module_name})
