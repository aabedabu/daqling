#pragma once

// 3rdparty or felix
#include "flxcard/FlxCard.h"
#include "cmem_rcc/cmem_rcc.h"
#include "packetformat/block_format.hpp"
#include <nlohmann/json.hpp>

// daqling
#include "Utils/ReusableThread.hpp"
#include "Core/Configuration.hpp"

// module
#include "FelixTypes.hpp"

#include <map>


/*
 * CardReader
 * Author: Roland.Sipos@cern.ch
 * Description: Reads FELIX Blocks in a scatter-gather fashion.
 *   The source is a given FlxCard instance and a DMA id.
 *   Pointers end up in a referenced queue.
 * Date: May 2020
*/
class CardReader
{
public:
  CardReader(felix::types::UniqueFlxCard& flxcard, std::mutex& cardmutex,
              unsigned cardno, unsigned dmaid, uint64_t memsize,
              std::map<uint64_t, felix::types::UniqueBlockPtrQueue>& blockpcqs
  );
  ~CardReader();

  // Control
  void configure(daqling::core::Configuration& /*config*/, felix::types::ElinkMetrics& /*em*/);
  void start();
  void stop();

private:
  // Card control
  felix::types::UniqueFlxCard& m_flx_card;
  std::mutex& m_card_mutex;

  // Constants
  static constexpr size_t M_MARGIN_BLOCKS=4;
  static constexpr size_t M_BLOCK_THRESHOLD=256;
  static constexpr size_t M_BLOCK_SIZE=felix::packetformat::BLOCKSIZE; 

  // Internals
  unsigned m_cardid;
  unsigned m_dmaid;
  uint64_t m_dma_memory_size;  // size of memory to allocate
  int m_cmem_handle;       // handle to the DMA memory block
  uint64_t m_virt_addr;    // virtual address of the DMA memory block
  uint64_t m_phys_addr;    // physical address of the DMA memory block
  uint64_t m_current_addr; // pointer to the current write position for the card
  unsigned m_read_index;
  u_long m_destination;    // FlxCard.h
  std::string m_infoStr;

  // Processor
  std::map<uint64_t, felix::types::UniqueBlockPtrQueue>& m_block_queues;
  daqling::utilities::ReusableThread m_dma_processor;
  std::atomic<bool> m_run_lock{false};
  std::atomic<bool> m_active{false};
  void processDMA();

  // Functionalities
  void setRunning(bool running);
  void openCard();
  void closeCard();
  int allocateCMEM(u_long bsize, u_long* paddr, u_long* vaddr);
  void initDMA();
  void startDMA();
  void stopDMA();
  uint64_t bytesAvailable();  
  void readCurrentAddress();

};
