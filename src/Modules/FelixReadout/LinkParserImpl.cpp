// From Daqling
#include "Utils/Logging.hpp"

// From Module
#include "LinkParserImpl.hpp"

// From STD
#include <iomanip>
#include <chrono>

using namespace std::chrono_literals;

LinkParserImpl::LinkParserImpl(unsigned id, unsigned linkId, unsigned queueId,
#ifndef BINARY_QUEUE
                             felix::types::UniqueFrameQueue& pcq
#else
                             felix::types::UniqueBinaryQueue& pcq
#endif
) 
    : m_id(id), m_linkId(linkId), m_queueId(queueId), m_hitsBindStr(""), m_pcq(pcq)
//    m_pcq{ QueueHandler::getInstance().getQueue(queueId) } 
// RS -> We don't queue in yet. If we won't be able to cope with the rate, 
//       we will re-introduce the queue and aggregate chunks for bigger ZMQ messages.
{
  std::thread statistics([this, id]()
  {
    auto newNow = std::chrono::high_resolution_clock::now();
    auto t0 = newNow;
    //unsigned numOps = 0;
    int newShorts = 0;
    int newChunks = 0;
    int newSubChunks = 0;
    int newBlocks = 0;
    int newErrorShorts = 0;
    int newErrorChunks = 0;
    int newErrorSubchunks = 0;
    int newErrorBlocks = 0;
    unsigned long lastChunkSize = 0;

    while (true)
    {
      auto newNow = std::chrono::high_resolution_clock::now();
      //numOps = m_packet_ctr.exchange(0);
      newShorts = m_short_ctr.exchange(0);
      newChunks = m_chunk_ctr.exchange(0);
      newSubChunks = m_subchunk_ctr.exchange(0);
      newBlocks = m_block_ctr.exchange(0);
      newErrorShorts = m_error_short_ctr.exchange(0);
      newErrorChunks = m_error_chunk_ctr.exchange(0);
      newErrorSubchunks = m_error_subchunk_ctr.exchange(0);
      newErrorBlocks = m_error_block_ctr.exchange(0);
      lastChunkSize = m_last_chunk_size.exchange(0);

      double seconds =  std::chrono::duration_cast<std::chrono::microseconds>(newNow-t0).count()/1000000.;
      INFO("LinkParserImpl[" << m_id << "," << m_linkId << "]"
        << " BlockRate [kHz] : " << newBlocks/seconds/1000. 
        << " ChunkRate [kHz] : " << newChunks/seconds/1000. 
        << " newShorts:" << newShorts << " newBlocks:" << newBlocks
        << " newChunks:" << newChunks << " newSubChunks:" << newSubChunks
        << " newErrorShorts:" << newErrorShorts << " newErrorChunks:" << newErrorChunks 
        << " newErrorSubchunks:" << newErrorSubchunks << " newErrorBlocks:" << newErrorBlocks
        << " lastChunkSize:" << lastChunkSize
      );

      std::this_thread::sleep_for(std::chrono::milliseconds(5000));
      t0 = newNow;
     }
  });
  statistics.detach();

}


LinkParserImpl::~LinkParserImpl()
{

}

void LinkParserImpl::log_packet(bool isShort)
{
  m_packet_ctr++;
  if (isShort)
    m_short_ctr++;
  else
    m_chunk_ctr++;

  if(m_packet_ctr == m_packet_target)
  {
    m_packet_target *= 10;
  }
}

void LinkParserImpl::dump_to_buffer(const char* data, long unsigned size,
                                   void* buffer, long unsigned buffer_pos, 
                                   const long unsigned& buffer_size)
{
  long unsigned bytes_to_copy = size;
  while(bytes_to_copy > 0)
  {
    unsigned int n = std::min(bytes_to_copy, buffer_size-buffer_pos);
    memcpy(static_cast<char*>(buffer)+buffer_pos, data, n);
    buffer_pos += n;
    bytes_to_copy -= n;
    if(buffer_pos == buffer_size)
    {
        buffer_pos = 0;
    }
  }
}

void LinkParserImpl::chunk_processed(const felix::packetformat::chunk& chunk)
{
  // Chunk size
  m_last_chunk_size = chunk.length();
  // Create binary placeholder
  //daqling::utilities::Binary cb(static_cast<void*>(chunk.data()), chunk.length());
  //m_cmgr.put(m_linkId, cb);

  if (m_overflow) {
    //std::this_thread::sleep_for(100us);
    return; 
  }

  long unsigned bytes_copied_chunk = 0;
  //long unsigned full_chunk_size = chunk.length();
  auto subchunk_data = chunk.subchunks();
  auto subchunk_sizes = chunk.subchunk_lengths();
  unsigned n_subchunks = chunk.subchunk_number();

#ifndef BINARY_QUEUE
// Flat buffer
  felix::types::SUPERCHUNK_CHAR_STRUCT superchunk;
  for(unsigned i=0; i<n_subchunks; i++)
  {
    dump_to_buffer(subchunk_data[i], subchunk_sizes[i], 
      static_cast<void*>(&superchunk), bytes_copied_chunk, felix::types::SUPERCHUNK_FRAME_SIZE);
    bytes_copied_chunk += subchunk_sizes[i];
  }
  m_pcq->write( std::move(superchunk) );
#else
// utils::Binary
  daqling::utilities::Binary cb(subchunk_data[0], subchunk_sizes[0]);
  for (unsigned i=1; i<n_subchunks; ++i) 
  {
    cb += daqling::utilities::Binary(subchunk_data[i], subchunk_sizes[i]);    
    bytes_copied_chunk += subchunk_sizes[i];
  }
  m_pcq->write(std::move(cb));
#endif

// ZMQ
/*
  zmq::message_t message(chunk.length());
  for(unsigned i=0; i<n_subchunks; ++i) {
    dump_to_buffer(subchunk_data[i], subchunk_sizes[i], message.data(), bytes_copied_chunk, full_chunk_size);
    bytes_copied_chunk += subchunk_sizes[i];
  }
  m_hitsq->write(std::move(message)); 
//*/
  //m_num_ops++;
  m_chunk_ctr++;
}

void LinkParserImpl::shortchunk_processed(const felix::packetformat::shortchunk& /*chunk*/)
{

  if (m_overflow) {
    return; 
  }

  //daqling::utilities::Binary sc(chunk.data, chunk.length);
  //m_pcq->write(std::move(sc));
  m_short_ctr++;
}

void LinkParserImpl::subchunk_processed(const felix::packetformat::subchunk& /*subchunk*/) {
  m_subchunk_ctr++;
}

void LinkParserImpl::block_processed(const felix::packetformat::block& /*block*/)
{
  if (!m_overflow) {
    if (m_pcq->isFull()) {
      m_overflow = true;
    }
  }

  m_block_ctr++;
}

void LinkParserImpl::chunk_processed_with_error(const felix::packetformat::chunk& /*chunk*/)
{
  m_error_chunk_ctr++;
}

void LinkParserImpl::subchunk_processed_with_error(const felix::packetformat::subchunk& /*subchunk*/)
{
  m_error_subchunk_ctr++;
}

void LinkParserImpl::shortchunk_process_with_error(const felix::packetformat::shortchunk& /*chunk*/)
{
  m_error_short_ctr++;
}

void LinkParserImpl::block_processed_with_error(const felix::packetformat::block& /*block*/)
{
  m_error_block_ctr++;
}
