/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Core/DAQProcess.hpp"
#include "Utils/Binary.hpp"
#include "Utils/ProducerConsumerQueue.hpp"

//#define BINARY_QUEUE

// Module
//#include "FelixReader.hpp"
#include "FelixTypes.hpp"
#include "CardReader.hpp"
#include "LinkReader.hpp"

class FelixReadoutModule : public daqling::core::DAQProcess 
{
                           //public daqling::core::DataProducer<felix::types::Frame> {
  void foobar(const std::string &arg);

public:
  FelixReadoutModule();
  ~FelixReadoutModule();

  void configure(); // optional (configuration can be handled in the constructor)
  void start(unsigned run_num);
  void stop();

private:
  void runner();
  void publish(unsigned chid);

  // Config
  uint8_t M_CHANNELS = 6;
  size_t M_BLOCK_QUEUE_SIZE = 6000000;
  size_t M_FRAME_QUEUE_SIZE = 800000;
  size_t M_CARD_DMA_MEMSIZE = 8192*1024*1024UL;

  uint8_t m_card_id = 0;
  uint8_t m_card_offset = 0;
  uint8_t m_dma_id = 0;
  uint8_t m_num_sources = 0;
  uint8_t m_num_links = 0;

  bool m_drain = false;

  // Thread control
  std::atomic<bool> m_stopParsers;
  std::atomic<bool> m_doPublish;

  // Metrics
  mutable std::map<uint64_t, felix::types::Metrics> m_elink_metrics;

  // Card control
  typedef std::unique_ptr<FlxCard> UniqueFlxCard;
  std::map<unsigned, UniqueFlxCard> m_flx_cards;
  std::mutex m_card_mutex;

  // Resources
  size_t m_frame_queue_size;
  std::map<uint64_t, felix::types::UniqueBlockPtrQueue> m_block_ptr_pcqs;
#ifndef BINARY_QUEUE
  std::map<uint64_t, felix::types::UniqueFrameQueue> m_frame_pcqs;
#else
  std::map<uint64_t, felix::types::UniqueBinaryQueue> m_frame_pcqs;
#endif

  // Parsers
  std::map<unsigned, std::map<uint32_t, uint32_t>> m_link_info;
  std::map<unsigned, std::unique_ptr<CardReader>> m_card_readers;
  std::map<unsigned, std::unique_ptr<LinkReader>> m_link_readers;
  std::map<unsigned, std::unique_ptr<daqling::utilities::ReusableThread>> m_link_pubs;

};
