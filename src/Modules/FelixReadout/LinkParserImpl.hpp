#pragma once

// 3rdparty, external
#include "packetformat/block_format.hpp"
#include "packetformat/block_parser.hpp"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

// From Daqling
#include "Utils/Logging.hpp"
#include "Utils/ProducerConsumerQueue.hpp"
#include "Core/ConnectionManager.hpp"

// From Module
#include "FelixTypes.hpp"

// From STD
#include <iomanip>


/*
 * LinkParserImpl
 * Author: Roland.Sipos@cern.ch
 * Description: Parser callbacks for FELIX chunks
 *   Implements ParserOperations from felix::packetformat
 * Date: May 2020
 * */

//template <class QT>
class LinkParserImpl : public felix::packetformat::ParserOperations
                    //: public DataProducerInterface<FT>
{
public:
  LinkParserImpl(unsigned id, unsigned linkId, unsigned queueId, // QT& pcq
#ifndef BINARY_QUEUE
                felix::types::UniqueFrameQueue& pcq
#else
                felix::types::UniqueBinaryQueue& pcq
#endif
  );
  ~LinkParserImpl();

  void free_zmq_msg(void *data, void *hint);
  void dump_to_buffer(const char* data, long unsigned size, 
                      void* buffer, long unsigned buffer_pos, 
                      const long unsigned& buffer_size);

  void log_packet(bool isShort);

  void chunk_processed(const felix::packetformat::chunk& chunk);
  void shortchunk_processed(const felix::packetformat::shortchunk& chunk); 
  void subchunk_processed(const felix::packetformat::subchunk& subchunk);
  void block_processed(const felix::packetformat::block& block);
  void chunk_processed_with_error(const felix::packetformat::chunk&); 
  void subchunk_processed_with_error(const felix::packetformat::subchunk& subchunk);
  void shortchunk_process_with_error(const felix::packetformat::shortchunk& chunk);
  void block_processed_with_error(const felix::packetformat::block& block);

private:
  std::atomic<unsigned> m_packet_ctr{0};
  std::atomic<int> m_short_ctr{0};
  std::atomic<int> m_chunk_ctr{0};
  std::atomic<int> m_subchunk_ctr{0};
  std::atomic<int> m_block_ctr{0};
  std::atomic<int> m_error_short_ctr{0};
  std::atomic<int> m_error_chunk_ctr{0};
  std::atomic<int> m_error_subchunk_ctr{0};
  std::atomic<int> m_error_block_ctr{0};
  std::atomic<unsigned long> m_last_chunk_size{0};

  bool error = false;
  uint32_t numErrors = 0;
  uint32_t numGood = 0;
  std::atomic<unsigned> m_errorsToReport{0};

  felix::packetformat::block m_prev_block;
  felix::packetformat::block m_err_block;

  unsigned m_id;
  unsigned m_packet_target = 1;
  unsigned m_linkId;
  unsigned m_queueId;
  std::string m_hitsBindStr;

  //FT& m_pcq;

#ifndef BINARY_QUEUE
  felix::types::UniqueFrameQueue& m_pcq;
#else
  felix::types::UniqueBinaryQueue& m_pcq;
#endif

  daqling::core::ConnectionManager &m_conns = daqling::core::ConnectionManager::instance(); 
  daqling::core::ConnectionManager::UniqueMessageQueue& m_hitsq = m_conns.getQueue(5);

  bool m_overflow;

};
