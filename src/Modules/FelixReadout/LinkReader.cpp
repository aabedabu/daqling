// 3rdparty or felix

// daqling
#include "Utils/Logging.hpp"
#include "Utils/ReusableThread.hpp"
#include "Core/Configuration.hpp"

// module
#include "LinkReader.hpp"

// std
#include <sstream>

using namespace std::chrono_literals;

LinkReader::LinkReader(unsigned cardid, unsigned link, unsigned tag,
                       felix::types::UniqueBlockPtrQueue& blockpcq,
#ifndef BINARY_QUEUE
                       felix::types::UniqueFrameQueue& framepcq
#else
                       felix::types::UniqueBinaryQueue& framepcq
#endif
) : 
  m_cardid(cardid), m_link(link), m_tag(tag), 
  m_block_queue(blockpcq), m_frame_queue(framepcq),
  m_link_parser_impl(m_cardid, m_link, m_tag, m_frame_queue), m_link_parser(m_link_parser_impl),
  m_link_processor{1}, m_run_lock{false}, m_active{false}
{
  std::ostringstream infoStream;
  infoStream << "LinkReader of card[" << m_cardid << "] link[" << m_link << "] tag[" << m_tag << "]";
  m_infoStr = infoStream.str();
  INFO(m_infoStr << " With source (BlockPtr) queue: " << &m_block_queue);
}


LinkReader::~LinkReader()
{
  INFO(m_infoStr << " is cleaning up...");
  if (m_active) {
    WARNING("LinkReader still active!? Attempting stop...");
    stop();
  }
  INFO(m_infoStr << " is done.");
}

void LinkReader::configure(daqling::core::Configuration& /*config*/, felix::types::ElinkMetrics& /*em*/)
{
  INFO(m_infoStr << " is configuring...");
}

void LinkReader::start()
{
  INFO(m_infoStr << " is starting...");
  if (!m_active.load()) {
    setRunning(true);
    m_link_processor.set_work(&LinkReader::processLink, this);
    INFO(m_infoStr << " is started.");    
  } else {
    WARNING(m_infoStr << " is already running!");
  }
}

void LinkReader::stop()
{
  INFO(m_infoStr << " is stopping...");
  if (m_active.load()) {
    setRunning(false);
    while(!m_link_processor.get_readiness()) {
      std::this_thread::sleep_for(10ms);
    }
    INFO(m_infoStr << " is stopped.");
  } else {
    WARNING(m_infoStr << " is already stopped!");
  }
}

void LinkReader::setRunning(bool shouldRun)
{
  bool wasRunning = m_active.exchange(shouldRun);
  DEBUG("Active state was toggled from " << wasRunning << " to " << shouldRun);
}

void LinkReader::processLink()
{
  INFO(m_infoStr << " spawning processLink() ...");
  int expected = -1;
  while (m_active) {
    uint64_t block_addr;
    if (m_block_queue->read(block_addr)) {
      const felix::packetformat::block* block = const_cast<felix::packetformat::block*>(
        felix::packetformat::block_from_bytes(reinterpret_cast<const char*>(block_addr))
      );
      if (expected >= 0) {
        if (expected != block->seqnr) {
          // Not expected sequence number! Expected is expected, but got block->seqnr
        }
      }
      expected = (block->seqnr + 1) % 32;
      m_link_parser.process(block);
    } else {
      std::this_thread::sleep_for(100us);
    }
  }
}
