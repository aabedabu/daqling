/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

#include "FelixReadoutModule.hpp"

#include <chrono>

using namespace daqling::core;
using namespace std::chrono_literals;

FelixReadoutModule::FelixReadoutModule() { 
  INFO(""); 
  m_doPublish = false;
}

FelixReadoutModule::~FelixReadoutModule() 
{
/*
  INFO("Clearing chunks readers..."); 
  m_chunk_readers.clear();
  INFO("Clearing block readers..."); 
  m_card_readers.clear();
  INFO("Clearing link information..."); 
  m_link_info.clear();
  INFO("Clearing flxCards..."); 
  m_flx_cards.clear();
*/
  INFO("Cleanup done."); 
}

// optional (configuration can be handled in the constructor)
void FelixReadoutModule::configure() {
  daqling::core::DAQProcess::configure();
  m_card_id = m_config.getSettings().value("card_id", 0);
  m_card_offset = m_config.getSettings().value("card_offset", 0);
  m_dma_id = m_config.getSettings().value("dma_id", 0);
  m_num_sources = m_config.getSettings().value("num_sources", 1);
  m_num_links = m_config.getSettings().value("num_links", M_CHANNELS);
  m_drain = m_config.getSettings().value("drain", false);
  m_frame_queue_size = static_cast<size_t>(m_config.getSettings().value("frame_queue_size", 1000000));
  INFO("");

  // Allocate and create necessary resources based on configuration
  for (uint64_t chid = 0; chid < m_num_links; chid++) {
    m_block_ptr_pcqs[chid] = std::make_unique<felix::types::BlockPtrQueue>(M_BLOCK_QUEUE_SIZE);
#ifndef BINARY_QUEUE
    m_frame_pcqs[chid] = std::make_unique<felix::types::FrameQueue>(m_frame_queue_size);
#else
    m_frame_pcqs[chid] = std::make_unique<felix::types::BinaryQueue>(m_frame_queue_size);
#endif
    m_elink_metrics[chid];
    INFO("Prepared queue for channel[" << chid << "] at " << &m_block_ptr_pcqs[chid]);
  }

  // Prepare link information. (Should this come from elinkconfig!? I think so!)
  for (unsigned i=0; i<m_num_sources; ++i) {
    unsigned cno = i+m_card_offset;
    INFO("Preparing links for source: " << i << " cardNo:" << cno);
    for (unsigned linkId=0; linkId<m_num_links; ++linkId) {
      unsigned tag = linkId*64+cno*2048;
      INFO(" -> linkId " << linkId << " tag:" << tag);
      m_link_info[cno][linkId] = tag;
      m_link_readers[linkId] = std::make_unique<LinkReader>(cno, linkId, tag, 
        m_block_ptr_pcqs[linkId], m_frame_pcqs[linkId]);

      m_link_pubs[linkId] = std::make_unique<daqling::utilities::ReusableThread>(linkId); 
    } 

    // Add multiple card source possibility here:
    m_flx_cards[cno] = std::make_unique<FlxCard>();
    m_card_readers[cno] = std::make_unique<CardReader>(
      m_flx_cards[cno], m_card_mutex, cno, m_dma_id, M_CARD_DMA_MEMSIZE, m_block_ptr_pcqs
    );
    m_card_readers[cno]->configure(m_config, m_elink_metrics); 
  }
  m_doPublish = false;
   
  // Instantiate dummy extractors (publishers)


  //registerCommand("foobar", "foobarred", &FelixReadoutModule::foobar, this, _1);
}

void FelixReadoutModule::start(unsigned run_num) {
  daqling::core::DAQProcess::start(run_num);
  for (auto const& [id, reader] : m_link_readers) {
    reader->start();
  } 
  for (auto const& [id, reader] : m_card_readers) {
    reader->start();
  }
  for (auto const& [id, pub] : m_link_pubs) {
    pub->set_work(&FelixReadoutModule::publish, this, id);
  }
  INFO("");
}

void FelixReadoutModule::stop() {
  daqling::core::DAQProcess::stop();
  for (auto const& [id, reader] : m_link_readers) {
    reader->stop();
  }
  for (auto const& [id, reader] : m_card_readers) {
    reader->stop();
  }
  for (auto const& [id, pub] : m_link_pubs) {
    while (!pub->get_readiness()){
      std::this_thread::sleep_for(1s);
    }
  }
  INFO("");
}

void FelixReadoutModule::publish(unsigned chid) {
  INFO("Publisher starts for channel: " << chid);
  while (!m_doPublish) {
    INFO("NOT YET!");
    std::this_thread::sleep_for(1s);
  }
  while(m_run) {
#ifndef BINARY_QUEUE
    felix::types::Frame sc;
#else
    daqling::utilities::Binary sc;
#endif
    if (m_frame_pcqs[chid]->read(std::ref(sc))) {
#ifndef BINARY_QUEUE
      auto binary = daqling::utilities::Binary(static_cast<const void *>(&sc), felix::types::FrameSize);
      if (!m_connections.put(chid, binary) || !m_run) {
#else
      if (!m_connections.put(chid, sc) || !m_run) {
#endif
        // Failed to publish.
        WARNING("Failed to publish!");
        std::this_thread::sleep_for(1s);
      }
      std::this_thread::sleep_for(1ms);
    } else {
      std::this_thread::sleep_for(1s);
    }
  }

/*
    zmq_msg_t hit_msg;
    rc = zmq_msg_init_data(&hit_msg, hits_payload, bytes_copied_chunk, free_zmq_msg, nullptr);
    assert(rc==0);
    rc = zmq_msg_send(&hit_msg, m_hits_socket, 0); // last part, as full chunk is serialized under hits_payload
    assert(rc!=-1);
*/

}


void FelixReadoutModule::runner() {
  INFO("Running..."); 
  // publish every data  
  while (m_run) {
    INFO("sizeGuess: " << m_frame_pcqs[0]->sizeGuess());
    if (m_frame_pcqs[0]->isFull()){
      INFO("Setting publish...");
      m_doPublish = true;
    } 
    std::this_thread::sleep_for(1s);
  }
  INFO("Runner stopped");
}


void FelixReadoutModule::foobar(const std::string &arg) {
  INFO("Inside custom command. Got argument: " << arg);
}
