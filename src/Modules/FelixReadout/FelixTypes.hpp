#pragma once

// 3rdparty, external
#include "packetformat/block_format.hpp"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

// Daqling
#include "Utils/ProducerConsumerQueue.hpp"
#include "Utils/Binary.hpp"

#include <map>

//#define BINARY_QUEUE

/*
 * Types
 * Author: Roland.Sipos@cern.ch
 * Description: 
 *   Collection of custom types used in the FELIX BR.
 * Date: May 2018
 */

namespace felix {
namespace types {

  typedef std::unique_ptr<FlxCard> UniqueFlxCard;

  static const size_t SUPERCHUNK_FRAME_SIZE = 5568; // for 12: 5568  for 6: 2784
  struct SUPERCHUNK_CHAR_STRUCT {
      char fragments[SUPERCHUNK_FRAME_SIZE];
  };
  typedef SUPERCHUNK_CHAR_STRUCT Frame;
  static const size_t FrameSize = SUPERCHUNK_FRAME_SIZE;

  typedef folly::ProducerConsumerQueue<SUPERCHUNK_CHAR_STRUCT> FrameQueue;
  typedef std::unique_ptr<FrameQueue> UniqueFrameQueue;
  typedef folly::ProducerConsumerQueue<daqling::utilities::Binary> BinaryQueue;
  typedef std::unique_ptr<BinaryQueue> UniqueBinaryQueue;

  // Meant to store pointers to FELIX Blocks 
  typedef folly::ProducerConsumerQueue<uint64_t> BlockPtrQueue;
  typedef std::unique_ptr<folly::ProducerConsumerQueue<uint64_t>> UniqueBlockPtrQueue;
  
  // Meant to store copies of FELIX Blocks
  typedef folly::ProducerConsumerQueue<felix::packetformat::block> BlockQueue;
  typedef std::unique_ptr<BlockQueue> UniqueBlockQueue; 

  // Meant to store FELIX chunks
  typedef folly::ProducerConsumerQueue<felix::packetformat::chunk> ChunkQueue;
  typedef std::unique_ptr<ChunkQueue> UniqueChunkQueue; 

  struct Metrics {
    std::atomic<size_t> queue_in_failure{0};
    std::atomic<size_t> something{0};
  };
  typedef std::map<uint64_t, Metrics> ElinkMetrics;

}
}

