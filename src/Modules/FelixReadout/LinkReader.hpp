#pragma once

// 3rdparty or felix
#include "flxcard/FlxCard.h"
#include "cmem_rcc/cmem_rcc.h"
#include "packetformat/block_format.hpp"
#include <nlohmann/json.hpp>

// daqling
#include "Utils/ReusableThread.hpp"
#include "Core/Configuration.hpp"

// module
#include "FelixTypes.hpp"
#include "LinkParserImpl.hpp"

#include <string>


/*
 * LinkReader
 * Author: Roland.Sipos@cern.ch
 * Description: Reads FELIX Chunks based on parser operations.
 * Date: May 2020
*/
class LinkReader
{
public:
  LinkReader(unsigned cardid, unsigned link, unsigned tag,
             felix::types::UniqueBlockPtrQueue& blockpcq,
#ifndef BINARY_QUEUE
             felix::types::UniqueFrameQueue& framepcq
#else
             felix::types::UniqueBinaryQueue& framepcq
#endif
  );
  ~LinkReader();

  // Control
  void configure(daqling::core::Configuration& /*config*/, felix::types::ElinkMetrics& /*em*/);
  void start();
  void stop();

private:
  // Funcitonalities
  void setRunning(bool shouldRun);

  // Internals
  unsigned m_cardid;
  unsigned m_link;
  unsigned m_tag;
  std::string m_infoStr;

  // Processor
  felix::types::UniqueBlockPtrQueue& m_block_queue;
#ifndef BINARY_QUEUE
  felix::types::UniqueFrameQueue& m_frame_queue;
#else
  felix::types::UniqueBinaryQueue& m_frame_queue;
#endif

//  typedef felix::packetformat::BlockParser<PacketHandler> RawParser;
  typedef felix::packetformat::BlockParser<LinkParserImpl> LinkParser;

  // Operations
//  PacketHandler m_packet_handler;
  LinkParserImpl m_link_parser_impl;
//  RawParser m_raw_parser;
  LinkParser m_link_parser;

  daqling::utilities::ReusableThread m_link_processor;
  std::atomic<bool> m_run_lock;
  std::atomic<bool> m_active;
  void processLink();
};
