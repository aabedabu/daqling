// 3rdparty, external
#include "flxcard/FlxException.h"

// From Daqling
#include "Utils/Logging.hpp"

// From Module
#include "CardReader.hpp"

// From Std
#include <random>
#include <chrono>
#include <unistd.h>
#include <iostream>
#include <sstream>

using namespace std::chrono_literals;

CardReader::CardReader(felix::types::UniqueFlxCard& flxcard, std::mutex& cardmutex, 
                         unsigned cardid, unsigned dmaid, uint64_t memsize,
                         std::map<uint64_t, felix::types::UniqueBlockPtrQueue>& blockpcqs)
  : m_flx_card(flxcard), m_card_mutex(cardmutex),
    m_cardid(cardid), m_dmaid(dmaid), m_dma_memory_size(memsize),
    m_block_queues(blockpcqs),
    m_dma_processor{1}
{
  std::ostringstream infoStream;
  infoStream << "CardReader of card[" << m_cardid << "] dmaid[" << m_dmaid + "] ";
  m_infoStr = infoStream.str();
  INFO(m_infoStr <<  " With CMEM size: " << m_dma_memory_size);
}

CardReader::~CardReader() 
{
  INFO(m_infoStr << " cleaning up...");
  if (m_active) {
    WARNING("We are still active? Attempting stop...");
    stop();
  }
  INFO(m_infoStr << " closing card...");
  closeCard();
}

void CardReader::configure(daqling::core::Configuration& /*config*/, felix::types::ElinkMetrics& /*em*/) 
{
  INFO("Configuring CardReader of card[" << m_cardid << "]");
  // Originalle here we created statistics threads, and dispatchers, the rest is as follows:
  // Open card
  openCard();
  // Allocate CMEM
  m_cmem_handle = allocateCMEM(m_dma_memory_size, &m_phys_addr, &m_virt_addr);  
  // Stop currently running DMA
  stopDMA();
  // Init DMA between software and card
  initDMA();
  // The rest was some CPU pinning.
}

void CardReader::start()
{
  INFO("Starting CardReader of card " << m_cardid << "...");
  if (!m_active.load()) { 
    startDMA();
    setRunning(true);
    m_dma_processor.set_work(&CardReader::processDMA, this);
    INFO("Started CardReader of card " << m_cardid << "...");
  } else {
    WARNING("CardReader of card " << m_cardid << " is already running!");
  }
}

void CardReader::stop()
{
  INFO("Stopping CardReader of card " << m_cardid << "...");
  if (m_active.load()) {
    stopDMA();
    setRunning(false);
    while (!m_dma_processor.get_readiness()) {
      std::this_thread::sleep_for(10ms);
    }
    INFO("Stopped CardReader of card " << m_cardid << "!"); 
  } else {
    WARNING("CardReader of card " << m_cardid << " is already stopped!"); 
  }
}

void CardReader::setRunning(bool shouldRun) 
{ 
  bool wasRunning = m_active.exchange(shouldRun);
  DEBUG("Active state was toggled from " << wasRunning << " to " << shouldRun);
}

void CardReader::openCard()
{
  INFO("Opening FELIX card " << m_cardid);
  try {
    m_card_mutex.lock();
    m_flx_card->card_open(static_cast<int>(m_cardid), LOCK_NONE);
    m_card_mutex.unlock();
  }
  catch(FlxException& ex) {
    ERROR("Exception thrown: " << ex.what());
    exit(EXIT_FAILURE);
  }
}

void CardReader::closeCard()
{
  INFO("Closing FELIX card " << m_cardid);
  try {
    m_card_mutex.lock();
    m_flx_card->card_close();
    m_card_mutex.unlock();
  }
  catch(FlxException& ex) {
    ERROR("Exception thrown: " << ex.what());
    exit(EXIT_FAILURE);
  }
}

int CardReader::allocateCMEM(u_long bsize, u_long* paddr, u_long* vaddr)
{
  INFO("Allocating CMEM buffer " << m_cardid << " dma id:" << m_dmaid);
  int handle;
  unsigned ret = CMEM_Open();
  if (!ret) {
    ret = CMEM_GFPBPASegmentAllocate(bsize, const_cast<char*>("FelixRO"), &handle); 
  }
  if (!ret) {
    ret = CMEM_SegmentPhysicalAddress(handle, paddr);
  }
  if (!ret) {
    ret = CMEM_SegmentVirtualAddress(handle, vaddr);
  }
  if (ret) {
    //rcc_error_print(stdout, ret);
    ERROR("Not enough CMEM memory allocated or the application demands too much CMEM memory."
       << "Fix the CMEM memory reservation in the driver or change the module's configuration.");
    m_card_mutex.lock();
    m_flx_card->card_close();
    m_card_mutex.unlock();
    exit(EXIT_FAILURE);
  }
  return handle;
}

void CardReader::initDMA()
{
  INFO("InitDMA issued...");
  m_card_mutex.lock();

  //m_flx_card->irq_disable();
  //std::cout <<"flxCard.irq_disable issued.");

  m_flx_card->dma_reset();
  DEBUG("flxCard.dma_reset issued.");

  m_flx_card->soft_reset();
  DEBUG("flxCard.soft_reset issued.");

  //flxCard.irq_disable(ALL_IRQS);
  //DEBUG("flxCard.irq_diable(ALL_IRQS) issued.");

  m_flx_card->dma_fifo_flush();
  DEBUG("flxCard.dma_fifo_flush issued.");

  //m_flx_card->irq_enable(IRQ_DATA_AVAILABLE);
  //DEBUG("flxCard.irq_enable(IRQ_DATA_AVAILABLE) issued.");
  m_card_mutex.unlock();
  
  m_current_addr = m_phys_addr;
  m_destination = m_phys_addr;
  m_read_index = 0;
  DEBUG("CardReader initialized card[" << m_cardid << "]");
}

void CardReader::startDMA()
{
  INFO("Issuing flxCard.dma_to_host for card " << m_cardid << " dma id:" << m_dmaid);
  m_card_mutex.lock();
  m_flx_card->dma_to_host(m_dmaid, m_phys_addr, m_dma_memory_size, FLX_DMA_WRAPAROUND); // FlxCard.h
  m_card_mutex.unlock();
} 

void CardReader::stopDMA()
{
  INFO("Issuing flxCard.dma_stop for card " << m_cardid << " dma id:" << m_dmaid);
  m_card_mutex.lock();
  m_flx_card->dma_stop(m_dmaid);
  m_card_mutex.unlock();
} 

uint64_t CardReader::bytesAvailable() 
{ 
  return (m_current_addr - ((m_read_index * M_BLOCK_SIZE) + m_phys_addr) + m_dma_memory_size) 
          % m_dma_memory_size;
}

void CardReader::readCurrentAddress() 
{
  m_card_mutex.lock();
  m_current_addr = m_flx_card->m_bar0->DMA_DESC_STATUS[m_dmaid].current_address;
  m_card_mutex.unlock();
}

void CardReader::processDMA()
{
  INFO("CardReader starts processing blocks...");
  while (m_active) {

    // Loop until read address makes sense
    while((m_current_addr < m_phys_addr) || (m_phys_addr+m_dma_memory_size < m_current_addr))
    {
      if (m_active.load()) {
        readCurrentAddress();
        std::this_thread::sleep_for(std::chrono::microseconds(5000)); //cfg.poll_time = 5000!!!
      } else {
        INFO("Stop issued during poll! Returning...");
        return;
      }
    }
    // Loop while there are not enough data
    while (bytesAvailable() < M_BLOCK_THRESHOLD*M_BLOCK_SIZE)
    {
      if (m_active.load()) {
        std::this_thread::sleep_for(std::chrono::microseconds(5000)); // cfg.poll_time = 5000
        readCurrentAddress();
      } else {
        INFO("Stop issued during poll! Returning...");
        return;
      }
    }

    // Set write index and start DMA advancing
    u_long write_index = (m_current_addr - m_phys_addr) / M_BLOCK_SIZE;
    uint64_t bytes = 0;
    while (m_read_index != write_index) {
      uint64_t fromAddress = m_virt_addr + (m_read_index * M_BLOCK_SIZE);

      // Interpret block
      const felix::packetformat::block* block = const_cast<felix::packetformat::block*>(
        felix::packetformat::block_from_bytes(reinterpret_cast<const char*>(fromAddress))
      );

      // Get ELink ID
      unsigned blockElink = static_cast<unsigned>(block->elink)/64;
      //INFO("BLOCK ELINK: " << blockElink);

      // Queue block pointer for processing
      if ( !m_block_queues[blockElink]->write(fromAddress) ) {
        //m_elink_metrics[block_elink]++;
        //ERROR("Could not queue in block for elink[" << blockElink << "]");
      }

      // Advance
      m_read_index = (m_read_index + 1) % (m_dma_memory_size / M_BLOCK_SIZE);
      bytes += M_BLOCK_SIZE; 
    }      

    // here check if we can move the read pointer in the circular buffer
    m_destination = m_phys_addr + (write_index * M_BLOCK_SIZE) - (M_MARGIN_BLOCKS * M_BLOCK_SIZE);
    if (m_destination < m_phys_addr) {
      m_destination += m_dma_memory_size;
    }

    // Finally, set new pointer
    m_card_mutex.lock();
    m_flx_card->dma_set_ptr(m_dmaid, m_destination);
    m_card_mutex.unlock();

  }
  INFO("CardReader processor thread finished.");
}

