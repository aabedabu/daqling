/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

/// \cond
#include <chrono>
#include <ctime>
#include <sstream>
/// \endcond

#include "PmemFelixModule.hpp"
#include "Utils/Logging.hpp"
#include "Utils/Common.hpp"

#include <libpmem.h>


/* size of the pmemblk pool -- 1 GB */
//#define POOL_SIZE ((off_t)(1 << 30))
#define POOL_SIZE 30ull*1024*1024*1024


/* size of each element in the pmem pool */
#define ELEMENT_SIZE 55680 



using namespace std::chrono_literals;
namespace daqutils = daqling::utilities;


// Set CPU affinity
void PmemFelixModule::SetAffinity(const size_t executorId) {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(executorId, &cpuset);
    int rc = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
       std::cerr << "Error calling pthread_setaffinity_np " << rc << "\n";
    }
}



char* PmemFelixModule::FileGenerator::next() {
  std::stringstream ss = daqutils::patternGenerator(m_filenum, m_run_number, m_chid, m_pattern);
  DEBUG("Next generated filename is: " << ss.str());  
  std::string pmemName = ss.str() ;

  // Creating a pmem file
  char* map_pmem; 
  if ((map_pmem = static_cast<char*>(pmem_map_file(pmemName.c_str(), POOL_SIZE,
         PMEM_FILE_CREATE|PMEM_FILE_EXCL, 0666, &m_mapped_len, &m_is_pmem))) == NULL) {
           perror("pmem_map_file");
           exit(1);
  }
  INFO("Created PMEM pool succesfully.");
                                                
  //return std::ofstream(ss.str(), std::ios::binary);
  return map_pmem;
}



bool PmemFelixModule::FileGenerator::yields_unique(const std::string &pattern) {
  std::map<char, bool> fields{{'n', false}, {'c', false}, {'D', false}, {'r', false}};
  for (auto c = pattern.cbegin(); c != pattern.cend(); c++) {
    if (*c == '%' && c + 1 != pattern.cend()) {
      try {
        fields.at(*(++c)) = true;
      } catch (const std::out_of_range &) {
        continue;
      }
    }
  }
  /*
   * While %D can conceptually yield unique files it has a resolution of 1s;
   * if FileGenerator::next() is called too often files will be overwritten.
   * Just to be sure, make sure every field is specified.
   */
  return std::all_of(fields.cbegin(), fields.cend(), [](const auto &f) { return f.second; });
}


// Constructor
PmemFelixModule::PmemFelixModule() : m_stopWriters{false} {
  DEBUG("");

  // Set up static resources...
  std::ios_base::sync_with_stdio(false);
}

// Destructor
PmemFelixModule::~PmemFelixModule() { DEBUG(""); }

void PmemFelixModule::configure() {
  DAQProcess::configure();
  // Read out required and optional configurations
  m_max_filesize = m_config.getSettings().value("max_filesize", 25 * daqutils::Constant::Giga);
  //m_buffer_size = m_config.getSettings().value("buffer_size", 4 * daqutils::Constant::Kilo);
  m_buffer_size = m_config.getSettings()["buffer_size"];
  m_queue_bunch = m_config.getSettings()["queue_bunch"];
  m_coreId = m_config.getSettings()["core_id"];
  m_channels = m_config.getConnections()["receivers"].size();
  m_pattern = m_config.getSettings()["filename_pattern"];
  INFO("Configuration:");
  INFO(" -> Maximum filesize: " << m_max_filesize << "B");
  INFO(" -> Buffer size: " << m_buffer_size << "B");
  INFO(" -> Queue PMEM bunch: " << m_queue_bunch);
  INFO(" -> Starting PMEM core: " << m_coreId);
  INFO(" -> channels: " << m_channels);

  if (!FileGenerator::yields_unique(m_pattern)) {
    CRITICAL("Configured file name pattern '"
             << m_pattern
             << "' may not yield unique output file on rotation; your files may be silently "
                "overwritten. Ensure the pattern contains all fields ('%c', '%n' and '%D').");
    throw std::logic_error("invalid file name pattern");
  }
  

  DEBUG("setup finished");

  // Contruct variables for metrics
  for (uint64_t chid = 0; chid < m_channels; chid++) {
    m_channelMetrics[chid];
  }

  if (m_statistics) {
    // Register statistical variables
    for (auto & [ chid, metrics ] : m_channelMetrics) {
      m_statistics->registerMetric<std::atomic<size_t>>(&metrics.bytes_written,
                                                        fmt::format("BytesWritten_chid{}", chid),
                                                        daqling::core::metrics::RATE);
      m_statistics->registerMetric<std::atomic<size_t>>(
          &metrics.payload_queue_size, fmt::format("PayloadQueueSize_chid{}", chid),
          daqling::core::metrics::LAST_VALUE);
      m_statistics->registerMetric<std::atomic<size_t>>(&metrics.payload_size,
                                                        fmt::format("PayloadSize_chid{}", chid),
                                                        daqling::core::metrics::AVERAGE);
    }
    DEBUG("Metrics are setup");
  }
}

void PmemFelixModule::start(unsigned run_num) {
  m_start_completed.store(false);
  DAQProcess::start(run_num);
  DEBUG(" getState: " << getState());

  m_stopWriters.store(false);
  unsigned int threadid = 11111;       // XXX: magic
  constexpr size_t queue_size = 10000; // XXX: magic

  for (uint64_t chid = 0; chid < m_channels; chid++) {
    // For each channel, construct a context of a payload queue, a consumer thread, and a producer
    // thread.
    std::array<unsigned int, 2> tids = {threadid++, threadid++};
    const auto & [ it, success ] =
        m_channelContexts.emplace(chid, std::forward_as_tuple(queue_size, std::move(tids)));
    assert(success);

    // Start the context's consumer thread.
    std::get<ThreadContext>(it->second)
        .consumer.set_work(&PmemFelixModule::flusher, this, it->first,
                           std::ref(m_connections.getQueue(it->first)), 
                           m_coreId + it->first, FileGenerator(m_pattern, it->first, m_run_number));

    //m_coreId += 1;

  }
  assert(m_channelContexts.size() == m_channels);

  m_monitor_thread = std::thread(&PmemFelixModule::monitor_runner, this);

  m_start_completed.store(true);
}

void PmemFelixModule::stop() {
  DAQProcess::stop();
  DEBUG(" getState: " << this->getState());
  m_stopWriters.store(true);
  for (auto & [ chid, ctx ] : m_channelContexts) {
    while (!std::get<ThreadContext>(ctx).consumer.get_readiness()) {
      std::this_thread::sleep_for(1ms);
    }
  }
  m_channelContexts.clear();

  if (m_monitor_thread.joinable()) {
    m_monitor_thread.join();
  }
}

void PmemFelixModule::runner() {
  DEBUG(" Running...");

  while (!m_start_completed) {
    std::this_thread::sleep_for(1ms);
  }
 

  // Start the producer thread of each context
  for (auto & [ chid, ctx ] : m_channelContexts) {
    std::get<ThreadContext>(ctx).producer.set_work([&]() {
      //auto &pq = std::get<PayloadQueue>(ctx); //FUTURE: comment

      while (m_run) {
        while (m_run) {
          if (m_statistics) {
            //m_channelMetrics.at(chid).payload_queue_size = pq.sizeGuess(); //FUTURE: comment
            //m_channelMetrics.at(chid).payload_queue_size = m_pcq->sizeGuess();
          }
          std::this_thread::sleep_for(1ms);
        }

      }
    });
  }

  while (m_run) {
    std::this_thread::sleep_for(1ms);
  };

  DEBUG(" Runner stopped");
}

void PmemFelixModule::flusher(const uint64_t chid, daqling::core::ConnectionManager::UniqueMessageQueue& m_pcq, 
                               const size_t coreId, FileGenerator fg) {

  PmemFelixModule::SetAffinity(coreId);

  size_t bytes_written = 0;
  char* buffer = fg.next();
  //auto buffer = daqutils::Binary();
  //char* payload = new char() ; 

  const auto pmemFlush = [&](char* &data, size_t dataSize) {
    try {
       pmem_memcpy_nodrain(buffer, data,  dataSize );
       buffer += dataSize ;
       m_channelMetrics.at(chid).bytes_written += dataSize ;
       bytes_written += dataSize;
    }  catch (...) {
       perror("");
       CRITICAL(" PMEM Write operation for channel " << chid << " failed.");
       exit(1);
    }
    //pmem_drain();
  };


  /* // Create a new PMEM pool once reached the maximum size
  if (bytes_written + max_buffer_size > m_max_filesize) { // Rotate output files
   INFO(" Rotating output files for channel " << chid);

   // Store the changes on persistent memory
   pmem_drain();

   // Free memory mapped file
   if (pmem_unmap(buffer, bytes_written) == -1) {
      perror("Error un-mapping PMEM file");
   }

   buffer = fg.next();
   bytes_written = 0;
   }
  */

  while (m_run) {
    while (!m_stopWriters) {
      while (m_pcq->isEmpty() && !m_stopWriters) { // wait until we have something to write
        std::this_thread::sleep_for(1ms);
      };
    zmq::message_t pl;
    unsigned int size_queue = m_pcq->sizeGuess();
    if (size_queue >= m_queue_bunch && size_queue > 1) {
        for (unsigned i=0; i<m_queue_bunch; i++) {
          if (m_pcq->read(std::ref(pl))) {
             if (m_statistics) {
               m_channelMetrics.at(chid).payload_size = pl.size();
             }
             // Persist the payload
             auto payload = reinterpret_cast<char*>(pl.data());
             pmemFlush(payload, pl.size());
          }
          else {
            DEBUG(" Cannot read payload on channel: " << chid);
          }
       }
    } else {
          DEBUG("Waiting to flush buffer.");
          std::this_thread::sleep_for(1s);
          m_channelMetrics.at(chid).payload_queue_size = m_pcq->sizeGuess();
 
    }
    }
  }


}

void PmemFelixModule::monitor_runner() {
  std::map<uint64_t, unsigned long> prev_value;
  while (m_run) {
    std::this_thread::sleep_for(1s);
    for (auto & [ chid, metrics ] : m_channelMetrics) {
      INFO("Bytes written (channel "
           << chid
           << "): " << static_cast<double>(metrics.bytes_written - prev_value[chid]) / 1000000
           << " MBytes/s " << "  Queue size "  << metrics.payload_queue_size );
      prev_value[chid] = metrics.bytes_written;
    }
  }
}


