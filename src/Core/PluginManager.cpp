/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

/// \cond
#include <chrono>
#include <ctime>
#include <iomanip>
#include <thread>
#include <algorithm>
/// \endcond

#include "Command.hpp"
#include "PluginManager.hpp"

using namespace daqling::core;
using namespace std::chrono_literals;

PluginManager::PluginManager() : m_loaded{false} 
{
  
}

PluginManager::~PluginManager() {
  m_loadeds.clear();
  m_modloaders.clear();
}

bool PluginManager::load(const std::string& name) {
  bool success = false;
  if ( std::find(m_loadeds.begin(), m_loadeds.end(), name) == m_loadeds.end() ){
    INFO("Loading Module[" << name << "]...");
    m_modloaders[name] = std::make_unique<ModuleLoader>();
    if (m_modloaders[name]->load(name)) {
      m_loadeds.emplace_back(name);
      success = true;
    } else {
      ERROR("Failed to load module: " << name);
      success = false;
    }
  } else {
    WARNING("Module[" << name << "] is already loaded!");
    success = false;
  }
  return success;
}

bool PluginManager::loadAll(const std::vector<std::string>& names) 
{
  INFO("Loading every module.");
  unsigned toload = names.size();
  for (auto name : names) {
    if (load(name))
      toload--;
  }
  m_loaded = (toload == 0) ? true : false;
  return m_loaded;
}

bool PluginManager::unload() {
  INFO("Unloading every module.");
  unsigned tounload = m_loadeds.size();
  for (auto const& modname : m_loadeds) {
    if (m_modloaders[modname]->unload()) {
      tounload--;
    } else {
      CRITICAL("Failed to unload module " << modname);
    }
  }
  return (tounload == 0) ? true : false;
}

void PluginManager::configure() {
  INFO("Configuring every module.");
  for (auto const& modname : m_loadeds) {
    m_modloaders[modname]->configure();
  }
}

void PluginManager::start(unsigned run_num) {
  INFO("Starting every module.");
  for (auto const& modname : m_loadeds) {
    m_modloaders[modname]->start(run_num);
  }
}

void PluginManager::stop() {
  INFO("Stoppping every module.");
  for (auto const& modname : m_loadeds) {
    m_modloaders[modname]->stop();
  }
}

bool PluginManager::command(const std::string& cmd, const std::string& arg) {
  INFO("Sending command for every module.");
  for (auto const& modname : m_loadeds) {
    m_modloaders[modname]->command(cmd, arg);
  }
  return true;
}

std::string PluginManager::getState(const std::optional<std::string>& mod = std::nullopt) {
  if (mod) {
    return m_modloaders[*mod]->getState();
  } else {
    // RS -> THIS IS SUPER BAD.
    return m_modloaders[m_loadeds[0]]->getState();
  }
}

bool PluginManager::isState(const std::string& state) {
  unsigned notinstate = m_loadeds.size();
  for (auto const& modname : m_loadeds) {
    if (m_modloaders[modname]->getState() == state) {
      notinstate--;
    }
  }
  return (notinstate == 0) ? true : false;
}

