/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Core/Configuration.hpp"

using namespace daqling::core;

template <typename T> 
void Configuration::set(const std::string &key, const T &value) 
{ 
  m_config[key] = value;
}

template <typename T> 
T Configuration::get(const std::string &key) 
{ 
  if ( m_config.contains(key) ) {
    return m_config[key]; 
  } else {
    WARNING("Configuration key[ " << key << "] doesn't exist");
    return T();
  }
}

// Explicit instanciation
template void Configuration::set<std::string>(const std::string& key, const std::string& value);
template void Configuration::set<unsigned>(const std::string& key, const unsigned& value);
template void Configuration::set<float>(const std::string& key, const float& value);

template unsigned Configuration::get<unsigned>(const std::string& key);
template float Configuration::get<float>(const std::string& key);
template std::string Configuration::get<std::string>(const std::string& key);
template std::vector<std::string> Configuration::get<std::vector<std::string>>(const std::string& key);

