/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file PluginManager.hpp
 * @brief Loads module (shared object) of type DAQProcess
 */

#ifndef DAQLING_CORE_PLUGINMANAGER_HPP
#define DAQLING_CORE_PLUGINMANAGER_HPP

/// \cond
#include <dlfcn.h>
#include <optional>
#include <string>
/// \endcond

#include "DAQProcess.hpp"
#include "ModuleLoader.hpp"

namespace daqling {
namespace core {

class PluginManager : public daqling::utilities::Singleton<PluginManager> {
private:
  std::map<std::string, std::unique_ptr<ModuleLoader>> m_modloaders;
  std::vector<std::string> m_loadeds;

  bool m_loaded;

public:
  PluginManager();
  ~PluginManager();

  /**
   * Tries to load a module of name `name`.
   * Returns whether the operation succeeded.
   */

  bool load(const std::string& name);

  bool loadAll(const std::vector<std::string>& names);

  /**
   * Unloads the loaded module.
   * Returns whether the operation succeeded.
   */
  bool unload();

  /**
   * Configures the loaded module.
   *
   * @warning May only be called after a successful `load`.
   */
  void configure();

  /**
   * Starts the loaded module.
   *
   * @warning May only be called after a successful `load`.
   */
  void start(unsigned run_num);

  /**
   * Stops the loaded module.
   *
   * @warning May only be called after a successful `load`.
   */
  void stop();

  /**
   * Executes a custom module command `cmd` if registered.
   *
   * Returns whether specified command was executed.
   */
  bool command(const std::string &cmd, const std::string &arg); 

  /**
   * Returns the state of the module.
   */
  std::string getState(const std::optional<std::string>&); 
  /**
   * Returns whether a module is loaded.
   */

  /**
   * Returns if the state of loaded modules is "state".
  */
  bool isState(const std::string& state);

  bool getLoaded() { return m_loaded; }
};

} // namespace core
} // namespace daqling

#endif // DAQLING_CORE_PLUGINMANAGER_HPP
